package com.example.XMCyberAssignment;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {

    @Bean
    public ActionBuilder actionBuilder() {
        // Initial action code and parameters can be null or empty, as they will be set later
        return new ActionBuilder();
    }

    @Bean
    public FileHandler fileHandler() {
        return FileHandler.getInstance();
    }
}
