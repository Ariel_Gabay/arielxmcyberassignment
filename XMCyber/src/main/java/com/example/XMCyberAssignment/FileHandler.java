package com.example.XMCyberAssignment;

import com.example.XMCyberAssignment.Constants;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class FileHandler {
    private static FileHandler instance;
    private FileWriter fileWriter;

    // Private constructor to prevent instantiation from outside
    private FileHandler() {
        // Initialize FileWriter
        try {
            File outputFile = new File(Constants.OUTPUT_FILE_PATH);
            if (!outputFile.exists()) {
                if (outputFile.createNewFile()) {
                    System.out.println(Constants.SUCCESSFUL_FILE_CREATION_MSG + outputFile.getName());
                }
            }
            this.fileWriter = new FileWriter(outputFile, true);
        } catch (IOException e) {
            System.out.println(Constants.FILE_CREATION_ERROR);
        }
    }

    // Static method to get the singleton instance
    public static synchronized FileHandler getInstance() {
        if (instance == null) {
            instance = new FileHandler();
        }
        return instance;
    }

    // Method to write message output to file
    public synchronized void writeMessageOutput(String msg) {
        try {
            this.fileWriter.write(msg + "\n"); // Write message to file with newline character
            this.fileWriter.flush(); // Flush the stream to ensure data is written immediately
        } catch (IOException e) {
            System.out.println(Constants.FILE_WRITE_ERROR + e.getMessage());
        }
    }

    // Method to close the file
    public synchronized void closeFile() {
        try {
            if (fileWriter != null) {
                fileWriter.close(); // Close the FileWriter
                System.out.println(Constants.SUCCESSFUL_FILE_CLOSE_MSG);
            }
        } catch (IOException e) {
            System.out.println(Constants.FILE_CLOSE_ERROR + e.getMessage());
        }
    }
}
