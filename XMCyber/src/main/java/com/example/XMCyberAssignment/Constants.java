package com.example.XMCyberAssignment;

public class Constants {

    //Action Codes
    public static final int CALC_FIB_CODE = 1;
    public static final int CALC_POL_DG1_CODE = 2;
    public static final int CALC_DERIVATIVE_CODE = 3;
    public static final int CALC_MEDIAN_CODE = 4;

    //Action Names
    public static final String CALC_FIB_STRING = "Calc Fibonacci result: ";
    public static final String CALC_POL_DG1_STRING = "Calc Polynomial result: ";
    public static final String CALC_DERIVATIVE_STRING = "Calc Derivative result: ";
    public static final String CALC_MEDIAN_STRING = "Calc Median result: ";
    //Action Parameter Counts
    public static final int CALC_FIB_PARAM_COUNT = 1;
    public static final int CALC_POL_DG1_PARAM_COUNT = 3;
    public static final int CALC_DERIVATIVE_PARAM_COUNT = 3;
    public static final int CALC_MEDIAN_PARAM_COUNT = 4;

    //Action Parameter input Strings
    public static final String CALC_FIB_PARAM_INPUT_STRING = "Please enter n value:";
    public static final String CALC_POL_DG1_PARAM_INPUT_STRING = "Please a, b, x values:";
    public static final String CALC_DERIVATIVE_PARAM_INPUT_STRING = "Please Enter at least 2 values for the A and B values and one for the X.\nENTER -1 TO FINISH INPUT.";
    public static final String CALC_MEDIAN_PARAM_INPUT_STRING = "Please Enter at least 2 values for median calculation.\nENTER -1 TO FINISH INPUT.";

    public static final int PARAMETERS_AMOUNT_INDEX = 0;
    public static final int PARAMETERS_INPUT_STRING_INDEX = 1;
    public static final int NUM_OF_ACTIONS = 4;

    public static final int MAX_PARAMETERS_ARRAY_SIZE = 100;
    //Polynomial Calculation Constants
    public static final int A_PARAM_INDEX = 0;
    public static final int X_PARAM_INDEX = 1;
    public static final int B_PARAM_INDEX = 2;

    public static final int MIN_POLYNOMIAL_PARAMS = 3;

    //Errors
    public static final String NEGATIVE_N_ERROR = "n must be a positive integer";
    public static final String INVALID_ARGS_AMOUNT_ERROR = "Polynomial must be of degree at least 1 and have X value";

    public static final String FILE_EXISTS_ERROR = "File already exists.";
    public static final String FILE_CREATION_ERROR = "File creation error occurred.";
    public static final String FILE_WRITE_ERROR = "File writing error occurred.";
    public static final String FILE_CLOSE_ERROR = "File close error occurred.";
    public static final String RUN_ACTION_ERROR = "Run action error: ";
    //File Constants
    public static final String OUTPUT_FILE_PATH = "MessageOutputs.txt";
    public static final String SUCCESSFUL_FILE_CREATION_MSG = "File created: ";
    public static final String SUCCESSFUL_FILE_CLOSE_MSG = "File closed successfully. ";

    public static final String INPUT_OUT_OF_RANGE_ERROR = "Please choose a number from 1 - 4.";


}

