package com.example.XMCyberAssignment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class XmCyberApplication {

	public static void main(String[] args) {
		SpringApplication.run(XmCyberApplication.class, args);
	}

}
