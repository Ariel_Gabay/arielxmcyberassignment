package com.example.XMCyberAssignment;

public class Message {
    private final int _actionCode;
    private final double[] _actionParams;

    //C'tor
    public Message(int actionCode, double[] actionParams) {
        this._actionCode = actionCode;
        this._actionParams = actionParams;
    }

    //Action Code Getter.
    public int getActionCode() {
        return this._actionCode;
    }

    //Action Parameters Getter.
    public double[] getActionParams() {
        return this._actionParams;
    }
}