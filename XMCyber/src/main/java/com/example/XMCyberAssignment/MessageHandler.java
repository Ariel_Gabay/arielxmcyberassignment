package com.example.XMCyberAssignment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MessageHandler {

    @Autowired
    private ActionBuilder actionBuilder;

    @Autowired
    private FileHandler fileHandler;

    //This function will get the message and a String with the action name,
    //run the action and write the result to the outputs file.
    public void handleMessage(Message message, String actionResultString) {
        try {
            // Get action code and parameters from the message
            int actionCode = message.getActionCode();
            double[] actionParams = message.getActionParams();

            // Set action code and parameters in the action builder
            actionBuilder.setActionCode(actionCode);
            actionBuilder.setActionParams(actionParams);

            // Get the appropriate action based on action code and execute it
            String result = actionResultString + actionBuilder.getAction().call();

            // Write the result to the output file
            fileHandler.writeMessageOutput(result);
            System.out.println(result);
        } catch (Exception e) {
            // Handle the exception (log it, rethrow it, etc.)
            System.err.println(Constants.RUN_ACTION_ERROR + e.getMessage());
        }
    }
}
