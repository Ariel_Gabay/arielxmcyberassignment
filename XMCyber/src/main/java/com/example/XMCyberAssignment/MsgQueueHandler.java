package com.example.XMCyberAssignment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Scanner;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Component
public class MsgQueueHandler implements CommandLineRunner {

    @Autowired
    private MessageHandler messageHandler;

    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
    private final Scanner in = new Scanner(System.in);

    //This function runs the queue
    @Override
    public void run(String... args) {
        scheduler.scheduleAtFixedRate(this::simulateIncomingMessages, 0, 1, TimeUnit.SECONDS);
    }

    //This function will input from the user the action code and return it.
    private int inputActionCode() {
        int input = 0;

        while (true) {
            System.out.println("Choose an action: \n1 - Fibonacci Calculation\n2 - Polynomial 1st Degree Calculation\n3 - Derivative Calculation\n4 - Median Calculation\n-1 - Exit");
            input = in.nextInt();

            if (input == -1) {
                scheduler.shutdown(); // Stop the scheduler when -1 is entered
                return -1; // Exit loop if user inputs -1
            } else if (input > 0 && input <= Constants.NUM_OF_ACTIONS) {
                break; // Exit loop if valid action code is entered
            } else {
                System.out.println(Constants.RUN_ACTION_ERROR);
            }
        }
        return input;
    }

    //This function will get the action code, input the parameters for the action and return them as an array of doubles.
    private double[] inputActionParameters(int actionCode) {
        int i = 0, parameterCount = Integer.parseInt(this.getParameters(actionCode)[Constants.PARAMETERS_AMOUNT_INDEX]);
        double[] params = new double[parameterCount];

        System.out.println("You chose option:" + this.getParameters(actionCode)[Constants.PARAMETERS_AMOUNT_INDEX] + this.getParameters(actionCode)[Constants.PARAMETERS_INPUT_STRING_INDEX]);
        if (actionCode != Constants.CALC_DERIVATIVE_CODE && actionCode != Constants.CALC_MEDIAN_CODE) {
            for (i = 0; i < parameterCount; i++) {
                System.out.println("Enter param: ");
                params[i] = in.nextDouble();
            }
        } else {
            params = new double[Constants.MAX_PARAMETERS_ARRAY_SIZE];
            double currentInput = 0.0;
            while (currentInput != -1) {
                System.out.println("Enter param: ");
                currentInput = in.nextDouble();
                params[i] = currentInput;
                i++;
            }
            params = Arrays.copyOf(params, i - 1);
        }
        return params;
    }

    //This function will get the action code and return an array of Strings, in the first
    //index there is the number of parameters required for the action and in the second index
    // there is a String with input instructions for the user.
    private String[] getParameters(int actionCode) {
        return switch (actionCode) {
            case Constants.CALC_FIB_CODE ->
                    new String[]{String.valueOf(Constants.CALC_FIB_PARAM_COUNT), Constants.CALC_FIB_PARAM_INPUT_STRING};
            case Constants.CALC_POL_DG1_CODE ->
                    new String[]{String.valueOf(Constants.CALC_POL_DG1_PARAM_COUNT), Constants.CALC_POL_DG1_PARAM_INPUT_STRING};
            case Constants.CALC_DERIVATIVE_CODE ->
                    new String[]{String.valueOf(Constants.CALC_DERIVATIVE_PARAM_COUNT), Constants.CALC_DERIVATIVE_PARAM_INPUT_STRING};
            case Constants.CALC_MEDIAN_CODE ->
                    new String[]{String.valueOf(Constants.CALC_MEDIAN_PARAM_COUNT), Constants.CALC_MEDIAN_PARAM_INPUT_STRING};
            default -> new String[]{"0"};
        };
    }

    //This function will get the action code and return the action string.
    private String getActionNameFromCode(int actionCode) {
        return switch (actionCode) {
            case Constants.CALC_FIB_CODE -> Constants.CALC_FIB_STRING;
            case Constants.CALC_POL_DG1_CODE -> Constants.CALC_POL_DG1_STRING;
            case Constants.CALC_DERIVATIVE_CODE -> Constants.CALC_DERIVATIVE_STRING;
            case Constants.CALC_MEDIAN_CODE -> Constants.CALC_MEDIAN_STRING;
            default -> "";
        };
    }

    //This function runs the queue of messages, it will input the data from the user and handle the messages.
    private void simulateIncomingMessages() {
        FileHandler fileHandler = FileHandler.getInstance();
        int actionCode;
        double[] actionParams;

        do {
            System.out.println("Enter -1 if you want to exit: ");

            actionCode = inputActionCode();
            if (actionCode == -1) {
                fileHandler.closeFile();
                return; // Exit the loop if -1 is entered

            }

            actionParams = inputActionParameters(actionCode);

            Message message = new Message(actionCode, actionParams);
            messageHandler.handleMessage(message, this.getActionNameFromCode(actionCode));
        } while (true); // Loop indefinitely until explicitly terminated
    }
}
