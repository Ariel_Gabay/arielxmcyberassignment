package com.example.XMCyberAssignment;

import com.example.XMCyberAssignment.ActionBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import java.util.concurrent.Callable;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class ActionBuilderTester {

    private ActionBuilder actionBuilder;

    @BeforeEach
    void setUp() {
        actionBuilder = new ActionBuilder();
    }

    //CALC_FIB Tester
    @Test
    void testGetActionForFibonacci() {
        actionBuilder.setActionCode(Constants.CALC_FIB_CODE);
        Callable<String> action = actionBuilder.getAction();
        assertEquals("55", executeAction(action, new double[]{10}), "Fibonacci result should be 55 for n=10");
    }

    //CALC_POL_DG1 Tester
    @Test
    void testGetActionForPolynomial() {
        actionBuilder.setActionCode(Constants.CALC_POL_DG1_CODE);
        Callable<String> action = actionBuilder.getAction();
        assertEquals("15.0", executeAction(action, new double[]{5, 2, 5}), "Polynomial result should be 15 for a=5, b=2, x=5");
    }

    //CALC_DERIVATIVE Tester.
    @Test
    void testGetActionForDerivative() {
        actionBuilder.setActionCode(Constants.CALC_DERIVATIVE_CODE);
        Callable<String> action = actionBuilder.getAction();
        assertEquals("10.0", executeAction(action, new double[]{1, 2, 3}), "Derivative result should be 10 for a=1, b=2, x=3");
    }

    //CALC_MEDIAN Tester.
    @Test
    void testGetActionForMedian() {
        actionBuilder.setActionCode(Constants.CALC_MEDIAN_CODE);
        Callable<String> action = actionBuilder.getAction();
        assertEquals("3.5", executeAction(action, new double[]{1, 2, 3, 4, 5, 6}), "Median result should be 3.5 for input array {1, 2, 3, 4, 5, 6}");
    }

    //This function will get a Callable(action) and an array of doubles(the parameters for the action)
    private String executeAction(Callable<String> action, double[] params) {
        try {
            actionBuilder.setActionParams(params);
            return action.call();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
