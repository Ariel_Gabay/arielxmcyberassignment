package com.example.XMCyberAssignment;

import java.util.Arrays;
import java.util.HashMap;
import java.util.concurrent.Callable;

public class ActionBuilder {

    private int _actionCode;
    private double[] _actionParams;
    private final HashMap<Integer, Callable<String>> _actionMap;

    //C'tor
    public ActionBuilder() {
        this._actionMap = new HashMap<>();
        this.createActionMap();
    }

    //Action Code Getter
    public int getActionCode() {
        return this._actionCode;
    }

    //Action parameters Getter
    public double[] getActionParams() {
        return this._actionParams;
    }

    public void setActionCode(int actionCode) {
        this._actionCode = actionCode;
    }

    //Action Parameters Setter
    public void setActionParams(double[] actionParams) {
        this._actionParams = actionParams;
    }

    //Action Getter from this._actionMap
    public Callable<String> getAction() {
        return this._actionMap.get(this._actionCode);
    }

    //Creating the action map for fast access to actions
    private void createActionMap() {
        this._actionMap.put(Constants.CALC_FIB_CODE, this::CALC_FIB);
        this._actionMap.put(Constants.CALC_POL_DG1_CODE, this::CALC_POL_DG1);
        this._actionMap.put(Constants.CALC_DERIVATIVE_CODE, this::CALC_DERIVATIVE);
        this._actionMap.put(Constants.CALC_MEDIAN_CODE, this::CALC_MEDIAN);
    }

    //This function will calculate the number in Fibonacci given its index(n).
    private String CALC_FIB() {
        int i = 0;
        long fib = 1, a = 1, b = 1;

        int n = (int) this._actionParams[0];

        if (n <= 0) {
            throw new IllegalArgumentException(Constants.NEGATIVE_N_ERROR);
        }

        if (n == 1 || n == 2) {
            return String.valueOf(1);
        }

        for (i = 0; i <= n; i++) {
            fib = a + b; // Fibonacci(n) = Fibonacci(n-1) + Fibonacci(n-2)
            a = b;
            b = fib;
        }

        return String.valueOf(fib);
    }

    //This function will calculate the result of a polynomial first degree given the coefficients and the X value.
    private String CALC_POL_DG1() {
        double a = this._actionParams[Constants.A_PARAM_INDEX], x = this._actionParams[Constants.X_PARAM_INDEX], b = this._actionParams[Constants.B_PARAM_INDEX];
        return String.valueOf(a * x + b);
    }

    //This function will calculate the derivative of a polynomial function given the coefficients and the X value.
    private String CALC_DERIVATIVE() {
        double derivative = 0;

        // Check if the polynomial is at least of degree 1

        if (this._actionParams.length < Constants.MIN_POLYNOMIAL_PARAMS) {
            throw new IllegalArgumentException(Constants.INVALID_ARGS_AMOUNT_ERROR);
        }

        // Calculate the derivative using the power rule
        for (int i = 1; i < this._actionParams.length - 2; i++) {
            derivative += this._actionParams[i] * i * Math.pow(this._actionParams[this._actionParams.length - 1], i - 1);
        }

        return String.valueOf(derivative);
    }

    //This function will calculate the median from a list of numbers.
    private String CALC_MEDIAN() {
        // Check if the array length is odd or even
        int n = this._actionParams.length;

        // Sort the array
        Arrays.sort(this._actionParams);

        if (n % 2 == 0) {
            // If even, average the middle two elements
            return String.valueOf((this._actionParams[n / 2 - 1] + this._actionParams[n / 2]) / 2.0);
        }
        // If odd, return the middle element
        return String.valueOf(this._actionParams[n / 2]);
    }
}
